﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;



namespace TwilioApp
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void SendMessage_OnClick(object sender, EventArgs e)
        {
            string ACCOUNT_SID = "AC74498a139c8353519f3c93a268aec6c2";
            string AUTH_TOKEN = "bd7756818fd5ae68e51f0bd8687609b2";

            TwilioClient.Init(ACCOUNT_SID, AUTH_TOKEN);

            var to = new PhoneNumber(ToNumber.Text);
            var from = new PhoneNumber("+16617504152");            
            var mediaUrl = new List<Uri>()
            {
                new Uri ("https://www.bridginghearts.org/images/hearts.png")
            };
            var message = Message.Text;

            MessageResource.Create(
                to: to,
                from: from,
                body: message,
                mediaUrl: mediaUrl);
        }
    }
}