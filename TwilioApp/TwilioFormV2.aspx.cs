﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace TwilioApp
{
    public partial class TwilioFormV2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SendMessage_OnClick(object sender, EventArgs e)
        {
            string ACCOUNT_SID = "AC74498a139c8353519f3c93a268aec6c2";
            string AUTH_TOKEN = "bd7756818fd5ae68e51f0bd8687609b2";

            TwilioClient.Init(ACCOUNT_SID, AUTH_TOKEN);

            try
            {
                var to = new PhoneNumber(ToNumber.Text);
                var from = new PhoneNumber("+16617504152");
                var message = Message.Text;
                var imagePath = ImagePath.Text;

                if (imagePath != "")
                {
                    var mediaUrl = new List<Uri>() {
                        new Uri(imagePath)
                    };

                    MessageResource.Create(
                        to: to,
                        from: from,
                        body: message,
                        mediaUrl: mediaUrl
                    );
                }
                else
                {
                    MessageResource.Create(
                        to: to,
                        from: from,
                        body: message
                    );
                }

                DisplayLabel.Text = "Your Message has been sent!";
            }
            catch (Exception ex)
            {
                DisplayLabel.Text = ex.ToString();
            }
        }
    }
}