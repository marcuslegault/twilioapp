﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="TwilioApp._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Twilio App | Version 1</title>
    <style>
        .big-link {
            background-color: #485563;
            font-size: 2em;
            color: #ffffff;
            font-weight: bold;
            text-transform: uppercase;
            text-decoration: none;
            border-color: rgba(0, 0, 0, 0);
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 8px 16px;
            line-height: 1.42857143;
            border-radius: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .big-link:hover {
            background-color: #df691a;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a class="big-link" runat="server" href="~/TwilioFormV2">Go to Version 2</a>
            <br />
            <br />
            <h3>To Number:</h3>
            <asp:TextBox ID="ToNumber" runat="server"></asp:TextBox>
            <div>(The phone number that will receive the message)</div>
            <br />
            <h3>SMS Message:</h3>
            <asp:TextBox ID="Message" runat="server"></asp:TextBox>
            <div>(The text that will be delivered)</div>
            <br />
            <br />
            <asp:Button ID="SendMessage" runat="server" Text="Send Message" OnClick="SendMessage_OnClick" />
        </div>
    </form>
</body>
</html>
