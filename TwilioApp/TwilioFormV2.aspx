﻿<%@ Page Title="Twilio Version 2" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TwilioFormV2.aspx.cs" Inherits="TwilioApp.TwilioFormV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="bs-docs-section form-group">
        <div class="row">
            <div class="col-lg-offset-3 col-lg-6 col-lg-offset-3">
                <div class="page-header">
                    <h1 id="forms">Send Texts Online</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-3 col-lg-6 col-lg-offset-3">
                <div class="well bs-component">
                    <h2>To Number:</h2>
                    <asp:TextBox ID="ToNumber" runat="server"></asp:TextBox>
                    <div>( The phone number that will receive the message )</div>
                    <br />
                    <h2>Message:</h2>
                    <asp:TextBox TextMode="MultiLine" height="100" class="form-control" ID="Message" runat="server"></asp:TextBox>
                    <div>( The text that will be delivered via SMS )</div>
                    <br />
                    <h2>Optional Image:</h2>
                    <asp:TextBox ID="ImagePath" runat="server"></asp:TextBox>
                    <div>( Enter the full URL to the .jpg/.png/.gif you would like to send )</div>
                    <br />
                    <asp:Button class="btn-primary btn-lg" ID="SendMessage" runat="server" Text="Send Message" OnClick="SendMessage_OnClick" />
                    <br />
                    <br />
                    <asp:Label class="display-text" ID="DisplayLabel" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
